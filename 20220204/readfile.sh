#!/bin/bash
# For reading a file line by line

filename=${1}
outputfile=${2}

if [ -f ${outputfile} ]; then
    echo "deleting existing file"
    rm ${outputfile}
fi

while read -r line; do
    echo $line
    firstemail=$(echo $line | cut -d"," -f4 | sed "s/yopmail/yahoomail/")
    secmail=$(echo $line | cut -d"," -f5)
    echo "${firstemail}, ${secmail}" >> ${outputfile}
done < "${filename}"
