#!/usr/bin/env bash

# infinate loop

i=0

while :
do
    (( i++ ))
    echo "Number : ${1}"
    echo "Press <CTRL+C> to exit."
    sleep 1
done
